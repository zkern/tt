﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class move3 : MonoBehaviour
{
    private Vector3 pos1 = new Vector3(6, 7, -15);
    private Vector3 pos2 = new Vector3(-6, 9, -15);
    public float speed = 1.0f;

    void Update()
    {
        transform.position = Vector3.Lerp(pos1, pos2, Mathf.PingPong(Time.time * speed, 2.0f));
    }
}