﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Destroy : MonoBehaviour
{

    public Text Score;
    private int Scor = 0;
    bool collision = false;

    void start()
    {
        Scor = 0;
        setScore();

    }

    private void setScore()
    {
        Scor = 0;
        Score.text = "Score: " + Scor;

    }
    void fixedUpdate()
    {
        if (collision == true)
        {
            Scor++;
            setScore();
        }

    }
    private void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.name == "Tower")
        {
            Destroy(col.gameObject);
            collision = true;
            Scor++;
        }
    }
}