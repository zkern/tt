﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Person2 : MonoBehaviour
{

    public GameObject projectilePrefab2;
    public Text countText;
    int max = 2;
    public Text display;
    private int count;

    // Use this for initialization
    void Start()
    {
        count = 0;
        setCountText();

    }

    private void setCountText()
    {
        if (count < max)
        {
            countText.text = "Heavy Ammo Fired: " + count;
        }
        else
        {
            countText.text = "You are out of Heavy Ammo!";
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    void LateUpdate()
    {


        float x = Input.GetAxis("Mouse X") * 2;
        float y = Input.GetAxis("Mouse Y");

        float yClamped = transform.eulerAngles.x + y;

        transform.rotation = Quaternion.Euler(yClamped,
            transform.eulerAngles.y, transform.eulerAngles.z);

        transform.RotateAround(new Vector3(0, 3, 0), Vector3.up, x);
    }

    void FixedUpdate()
    {
        if (count < max)
        {
            if (Input.GetButtonDown("Fire1"))
            { //Right click. use Fire1 for left click
                GameObject projectile = Instantiate(projectilePrefab2,
                                            transform.position, transform.rotation) as GameObject;

                Rigidbody rb = projectile.GetComponent<Rigidbody>();
                rb.AddRelativeForce(projectile.transform.forward * 500000);

                count++;
                setCountText();
            }
        }
        else
        {
            display.text = "out of ammo";
        }
    }

}

