﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class move4 : MonoBehaviour
{
    private Vector3 pos1 = new Vector3(-6, 6, -12);
    private Vector3 pos2 = new Vector3(6, 9, -12);
    public float speed = 3.0f;

    void Update()
    {
        transform.position = Vector3.Lerp(pos1, pos2, Mathf.PingPong(Time.time * speed, 1.5f));
    }
}